import yaml

with open("./cf-test/template.yml", 'r') as f:
    try:
        print(yaml.load(f))
    except yaml.YAMLError as exc:
        print(exc)
